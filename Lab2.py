

dic = {}


def menu2 ():
    """
    Para el menu 2 que servira para todas las opciones
    """
    menu2 = int(input("\n1. Regresar al menu principal\n"
              "2. Continuar\n"
              "Ingrese una opcion : \n"))
    if menu2 == 1:
        menu()
    elif menu2 ==2:
        if menu == 1:
            pass
        elif menu ==2:
            recorrer()
        elif menu ==3:
            eliminar()
        elif menu ==4:
            consultar()
        elif menu == 5:
            cambiar()
        elif menu == 6:
            doc()

def agregar ():
    """
    Para ingresar un usuario al diccionario
    """
    cantidad = int(input("Cuantos usuarios desea ingresar? : "))
    for i in range (0,cantidad):
        cedula = int(input("\nIngrese numero de cedula : "))
        nombre = input("Ingrese nombre : ")
        dic[cedula]=nombre
    menu2()


def doc ():
    print(menu.__doc__)
    print(menu2.__doc__)
    print(agregar.__doc__)
    print(recorrer.__doc__)
    print(eliminar.__doc__)
    print(consultar.__doc__)
    print(cambiar.__doc__)
    menu2()







def recorrer ():
    """
    Imprime todos los usuarios que hayan en el diccionario
    """
    for key in dic:
        print(key, ":", dic[key])



def eliminar ():
    """
    Elimina a un usurio ya entes introducido por medio del numero de cedula
    """
    cedula = int(input("Ingrese numero de cedula : "))
    if cedula in dic:
        dic.pop(cedula)
        menu2()
    else:
        print("Numero de cedula no se encuentra!")


def consultar ():
    """
    Para consultar el nombre de una persona por numero cedula
    """
    cedula = int(input("Ingrese numero de cedula : "))
    print("El nombre de la persona de ese numero de cedula es {}".format(dic.get(cedula)))
    menu2()


def cambiar ():
    """
    Para cambiar nombre por numero de cedula
    """
    cedula = int(input("Ingrese numero de cedula : "))
    if cedula in dic:
        nombre1 = input("Ingrese un nuevo nombre : ")
        dic[cedula]=nombre1
        menu2()
    else:
        print("La cedula ingresada no se encuentra!")




def menu ():
    """
    Menu principal
    """
    while(True):
        try:
            menu = int(input(">>----REGISTRO UTN----<<\n"
                "1. Ingresar persona\n"
                "2. Recorrer nombres registrados\n"
                "3. Eliminar persona por cedula\n"
                "4. Consultar por numero de cedula\n"
                "5. Cambiar nombre por cedula\n"
                "6. Imprimir documentacion\n"
                "7. Salir\n"
                "Digite una opcion : \n"))


            while menu ==1:
              agregar()

            if menu == 2:
              recorrer()
              menu2()
            elif menu ==3:
              eliminar()
            elif menu == 4:
              consultar()
            elif menu == 5:
              cambiar()
            elif menu == 6:
              doc()
            elif menu ==7:
                print("Gracias por usar la aplicacion!")
                break
            else:
                    print("Numero no valido!")
        except:
            print("\nHa introducido un valor incorrecto !")
menu()



